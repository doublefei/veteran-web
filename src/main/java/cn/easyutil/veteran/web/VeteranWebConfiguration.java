package cn.easyutil.veteran.web;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Configuration
@ConfigurationProperties(prefix = "veteran.web")
public class VeteranWebConfiguration {

    /** 是否开启返回异常堆栈信息*/
    private boolean enableErrorTrace = true;

    /** 日志打印是否要包含请求参数*/
    private boolean logWithRequest = true;

    /** 日志打印是否要包含响应参数*/
    private boolean logWithResponse = true;

    /** 日志打印是否要包含详细的子类用户信息*/
    private boolean logWithDetailUser = false;

    /** 指定打印的用户账号，调试时可针对固定账号开启，为空时认为不指定*/
    private String logByAssignAccount = "";

    /** token在header中的名字*/
    private String tokenName = "token";

    /** 是否使用默认用户信息*/
    private boolean useDefaultUser = false;

    /** 默认用户id*/
    private Long defaultUserId = 1L;

    /** 默认用户账号*/
    private String defaultUserAccount = "root";

    /** 默认用户的加密密钥*/
    private String defaultUserSecret = "1234567890123456";

    /** 需要校验登陆的接口*/
    private Set<String> includeAuthUri = new HashSet<>();

    /** 不需要校验登陆的接口*/
    private Set<String> excludeAuthUri = new HashSet<>();

    /** 需要被包装返回值的接口*/
    private Set<String> includePackageUri = new HashSet<>();

    /** 不需要被包装返回值的接口*/
    private Set<String> excludePackageUri = new HashSet<>();

    /** 需要进行加解密处理的接口*/
    private Set<String> includeSecretUri = new HashSet<>();

    /** 不需要进行加解密处理的接口*/
    private Set<String> excludeSecretUri = new HashSet<>();

    /** 需要打印日志的接口*/
    private Set<String> includeLogUri = new HashSet<>();

    /** 不需要打印日志的接口*/
    private Set<String> excludeLogUri = new HashSet<>();

    public boolean isEnableErrorTrace() {
        return enableErrorTrace;
    }

    public void setEnableErrorTrace(boolean enableErrorTrace) {
        this.enableErrorTrace = enableErrorTrace;
    }

    public boolean isLogWithRequest() {
        return logWithRequest;
    }

    public void setLogWithRequest(boolean logWithRequest) {
        this.logWithRequest = logWithRequest;
    }

    public boolean isLogWithResponse() {
        return logWithResponse;
    }

    public void setLogWithResponse(boolean logWithResponse) {
        this.logWithResponse = logWithResponse;
    }

    public boolean isLogWithDetailUser() {
        return logWithDetailUser;
    }

    public void setLogWithDetailUser(boolean logWithDetailUser) {
        this.logWithDetailUser = logWithDetailUser;
    }

    public String getLogByAssignAccount() {
        return logByAssignAccount;
    }

    public void setLogByAssignAccount(String logByAssignAccount) {
        this.logByAssignAccount = logByAssignAccount;
    }

    public String getTokenName() {
        return tokenName;
    }

    public void setTokenName(String tokenName) {
        this.tokenName = tokenName;
    }

    public Long getDefaultUserId() {
        return defaultUserId;
    }

    public void setDefaultUserId(Long defaultUserId) {
        this.defaultUserId = defaultUserId;
    }

    public Set<String> getIncludeAuthUri() {
        return includeAuthUri;
    }

    public void setIncludeAuthUri(Set<String> includeAuthUri) {
        this.includeAuthUri = includeAuthUri;
    }

    public Set<String> getExcludeAuthUri() {
        return excludeAuthUri;
    }

    public void setExcludeAuthUri(Set<String> excludeAuthUri) {
        this.excludeAuthUri = excludeAuthUri;
    }

    public Set<String> getIncludePackageUri() {
        return includePackageUri;
    }

    public void setIncludePackageUri(Set<String> includePackageUri) {
        this.includePackageUri = includePackageUri;
    }

    public Set<String> getExcludePackageUri() {
        return excludePackageUri;
    }

    public void setExcludePackageUri(Set<String> excludePackageUri) {
        this.excludePackageUri = excludePackageUri;
    }

    public Set<String> getIncludeSecretUri() {
        return includeSecretUri;
    }

    public void setIncludeSecretUri(Set<String> includeSecretUri) {
        this.includeSecretUri = includeSecretUri;
    }

    public Set<String> getExcludeSecretUri() {
        return excludeSecretUri;
    }

    public void setExcludeSecretUri(Set<String> excludeSecretUri) {
        this.excludeSecretUri = excludeSecretUri;
    }

    public Set<String> getIncludeLogUri() {
        return includeLogUri;
    }

    public void setIncludeLogUri(Set<String> includeLogUri) {
        this.includeLogUri = includeLogUri;
    }

    public Set<String> getExcludeLogUri() {
        return excludeLogUri;
    }

    public void setExcludeLogUri(Set<String> excludeLogUri) {
        this.excludeLogUri = excludeLogUri;
    }

    public String getDefaultUserAccount() {
        return defaultUserAccount;
    }

    public void setDefaultUserAccount(String defaultUserAccount) {
        this.defaultUserAccount = defaultUserAccount;
    }

    public boolean isUseDefaultUser() {
        return useDefaultUser;
    }

    public void setUseDefaultUser(boolean useDefaultUser) {
        this.useDefaultUser = useDefaultUser;
    }

    public String getDefaultUserSecret() {
        return defaultUserSecret;
    }

    public void setDefaultUserSecret(String defaultUserSecret) {
        this.defaultUserSecret = defaultUserSecret;
    }
}
