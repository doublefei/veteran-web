package cn.easyutil.veteran.web.provider;

import cn.easyutil.veteran.core.entity.SessionUser;
import cn.easyutil.veteran.core.logger.LoggerUtil;
import cn.easyutil.veteran.core.utils.AssertUtil;
import cn.easyutil.veteran.web.advice.BeforeRequestAdvice;
import cn.easyutil.veteran.web.handler.BeforeRequestHandle;
import cn.easyutil.veteran.web.session.SessionOperation;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.util.PathMatcher;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Optional;
import java.util.Set;

public class DefaultBeforeRequestAdvice implements BeforeRequestAdvice {

    @Override
    public void beforeAuth(HttpServletRequest request) {
        String contentType = request.getContentType();
        if(StringUtils.isNotBlank(contentType) && !contentType.toLowerCase().contains("json")){
            if(SessionOperation.toLog.get()){
                LoggerUtil.info(this.getClass(),"收到【{}】接口请求",request.getRequestURI());
            }
        }
    }

    @Override
    public void afterAuth(HttpServletRequest request,SessionUser user) {
        Set<String> apiPathSet = user.getApiPaths();
        if(!CollectionUtils.isEmpty(apiPathSet)){
            String requestURI = request.getRequestURI();
            PathMatcher matcher = new AntPathMatcher();
            boolean match = apiPathSet.stream().anyMatch(path -> matcher.match(path, requestURI));
            AssertUtil.isTrue(!match,"暂无接口访问权限");
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, Method method) {
        if(SessionOperation.toLog.get()){
            SessionUser user = SessionOperation.getUserNotException();
            SessionUser copy = new SessionUser();
            if(user != null){
                BeanUtils.copyProperties(user,copy);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("请求接口:").append(SessionOperation.getRequest().getRequestURI()).append(",");
            if(SessionOperation.getConfiguration().isLogWithDetailUser()){
                sb.append("当前用户:").append(JSONUtil.toJsonStr(user)).append(",");
            }else{
                sb.append("当前用户:").append(JSONUtil.toJsonStr(copy)).append(",");
            }
            if(SessionOperation.getConfiguration().isLogWithRequest()){
                Object body = Optional.ofNullable(SessionOperation.requestParams.get()).orElse(new Object());
                sb.append("请求参数:").append(JSONUtil.toJsonStr(body)).append(",");
            }
            if(SessionOperation.getConfiguration().isLogWithResponse()){
                Object responseBody = Optional.ofNullable(SessionOperation.responseParams.get()).orElse(new Object());
                sb.append("响应参数:").append(JSONUtil.toJsonStr(responseBody));
            }
            LoggerUtil.info(BeforeRequestHandle.class, sb.toString());
        }
    }
}
