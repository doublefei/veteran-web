package cn.easyutil.veteran.web.provider;

import cn.easyutil.veteran.core.entity.ResponseEntity;
import cn.easyutil.veteran.core.error.CommonException;
import cn.easyutil.veteran.core.error.ErrorEnum;
import cn.easyutil.veteran.web.advice.GlobalExceptionAdvice;
import cn.easyutil.veteran.web.session.SessionOperation;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

public class DefaultGlobalExceptionAdvice implements GlobalExceptionAdvice {

    @Override
    public boolean printLog(Exception e) {
        return !(e instanceof BindException) && !(e instanceof CommonException);
    }

    @Override
    public Object packageResponse(Exception e) {
        ResponseEntity<?> result = ResponseEntity.error(e.getMessage());
        // 处理dto验证异常
        if (e instanceof BindException) {
            BindException ex = (BindException) e;
            FieldError fieldError = ex.getBindingResult().getFieldError();
            if(fieldError != null){
                result.setErrorMsg(fieldError.getField() + "：" + fieldError.getDefaultMessage());
            }
        }
        // 处理自定义异常
        else if (e instanceof CommonException) {
            CommonException ex = (CommonException) e;
            result.setCode(ex.getErrorCode().getCode());
            result.setErrorMsg(ex.getMessage());
            result.setErrorMsgDesc(ex.getRemark());
        }
        else {
            result.setErrorMsg(ErrorEnum.error.getRemark());
            result.setErrorMsgDesc(e.getMessage());
        }
        if(SessionOperation.toTrance.get()!=null && !SessionOperation.toTrance.get()){
            result.setErrorMsgDesc(null);
        }
        return result;
    }
}
