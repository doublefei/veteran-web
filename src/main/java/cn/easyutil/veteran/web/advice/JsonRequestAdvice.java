package cn.easyutil.veteran.web.advice;

import com.fasterxml.jackson.databind.JavaType;

import java.lang.reflect.Type;

public interface JsonRequestAdvice {

    /**
     * 解析请求参数
     * @param text  读取到的原始参数
     * @param javaType  java类型
     * @param type  接口方法参数类型
     * @param controllerClass controller类
     * @return  最终处理后的请求参数
     */
    String parseBody(String text, JavaType javaType,Type type, Class<?> controllerClass);

    /**
     * 执行接口方法前的处理
     * @param body  请求参数对象
     * @return  请求参数对象
     */
    Object beforeInvocation(Object body);
}
