package cn.easyutil.veteran.web.advice;

import org.springframework.core.MethodParameter;

import java.lang.reflect.Method;

public interface BeforeResponseAdvice {

    /**
     * 是否处理当前请求
     */
    boolean supports(MethodParameter returnType, Class converterType);

    /**
     * 包装返回数据
     * @param body  原始返回数据
     * @param method    接口方法
     * @return  包装后的返回数据
     */
    Object toPackage(Object body, Method method);

    /**
     * 对包装数据进行加密
     * @param body  包装后的返回数据
     * @param method    接口方法
     */
    Object toSecret(Object body,Method method);

    /**
     * 所有处理完成后返回客户端前的处理
     * @param body  将要返回的数据
     * @param method    接口方法
     * @return  最终要返回的数据
     */
    Object beforeBodyWrite(Object body,Method method);
}
