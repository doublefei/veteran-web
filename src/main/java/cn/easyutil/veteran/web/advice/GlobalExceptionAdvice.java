package cn.easyutil.veteran.web.advice;

public interface GlobalExceptionAdvice {

    boolean printLog(Exception e);

    Object packageResponse(Exception e);
}
