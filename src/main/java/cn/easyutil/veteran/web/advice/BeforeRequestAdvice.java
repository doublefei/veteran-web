package cn.easyutil.veteran.web.advice;

import cn.easyutil.veteran.core.entity.SessionUser;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

public interface BeforeRequestAdvice {

    void beforeAuth(HttpServletRequest request);

    void afterAuth(HttpServletRequest request, SessionUser user);

    void afterCompletion(HttpServletRequest request, Method method);
}
