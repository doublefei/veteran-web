package cn.easyutil.veteran.web.session;

import cn.easyutil.veteran.core.entity.SessionUser;

public interface SessionUserFetch<T extends SessionUser> {

    /**
     * 获取当前登录的用户
     * @param token token
     */
    T getUser(String token);

    /**
     * 保存用户信息
     */
    void setUser(T user);

    /**
     * 修改用户信息
     */
    void updateUser(T user);

    /**
     * 刷新缓存
     */
    void refresh(String token);

}
