package cn.easyutil.veteran.web.session;

import cn.easyutil.veteran.core.entity.BaseEntity;
import cn.easyutil.veteran.core.logger.LogLevel;

public class SessionLogStorage extends BaseEntity {

    private LogLevel level;

    private Class<?> clazz;

    private String msg;

    private Object[] args;

    public LogLevel getLevel() {
        return level;
    }

    public void setLevel(LogLevel level) {
        this.level = level;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }
}
