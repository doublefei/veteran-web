package cn.easyutil.veteran.web.handler;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.NumberSerializer;

import java.io.IOException;

public class BigLongTypeSerializer extends NumberSerializer {

    public final static BigLongTypeSerializer instance = new BigLongTypeSerializer(Long.class);

    protected BigLongTypeSerializer(Class t) {
        super(t);
    }

    @Override
    public void serialize(Number value, JsonGenerator g, SerializerProvider provider) throws IOException {
        if(value instanceof Long){
            Long val = (Long) value;
            if(val.toString().length() > 15){
                g.writeString(val.toString());
                return;
            }
        }
        super.serialize(value, g, provider);
    }
}
