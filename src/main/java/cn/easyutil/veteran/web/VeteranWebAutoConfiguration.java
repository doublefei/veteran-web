package cn.easyutil.veteran.web;

import cn.easyutil.veteran.core.logger.LoggerUtil;
import cn.easyutil.veteran.web.log.VeteranWebLogInterceptor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * 自动装配
 */
@ComponentScan(basePackages = "cn.easyutil.veteran.web")
@Configuration
public class VeteranWebAutoConfiguration implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        LoggerUtil.addInterceptor(new VeteranWebLogInterceptor());
    }
}
