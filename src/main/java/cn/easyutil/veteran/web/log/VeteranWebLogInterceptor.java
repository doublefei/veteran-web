package cn.easyutil.veteran.web.log;

import cn.easyutil.veteran.core.logger.LogLevel;
import cn.easyutil.veteran.core.logger.LoggerPrintInterceptor;
import cn.easyutil.veteran.web.session.SessionOperation;

public class VeteranWebLogInterceptor implements LoggerPrintInterceptor {

    @Override
    public boolean isPrint(LogLevel logLevel, Class<?> aClass, String s, Object... objects) {
        return SessionOperation.toLog.get() && aClass.getCanonicalName().contains("cn.easyutil.veteran.web");
    }
}
